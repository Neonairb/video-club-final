FROM node 
LABEL Brian Acosta
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start