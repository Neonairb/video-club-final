module.exports = (sequelize, type) => {
    const Genre = sequelize.define('genres', {
        id: {type: type.INTEGER, primaryKey: true, autoIncrement: true},
        descriptions: type.STRING,
        status: type.BOOLEAN
    });
    return Genre;
};