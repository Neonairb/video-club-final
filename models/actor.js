module.exports = (sequelize, type) => {
    const Actor = sequelize.define('actors', {
        id: {type: type.INTEGER, primaryKey: true, autoIncrement: true},
        date: type.STRING
    });
    return Actor;
};