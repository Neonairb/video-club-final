const Sequelize = require('sequelize');

const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const copyModel = require('./models/copy');
const bookingModel = require('./models/booking');
const actorModel = require('./models/actor');
const directorModel = require('./models/director');
const memberModel = require('./models/member');
const movieActorModel = require('./models/movieActor');

// 1) Nombre de la base de datos
// 2) Usuario de la base de datos
// 3) Contrasena de la base de datos
// 4) Objeto de configuracion (ORM)

const sequelize = new Sequelize('video_club', 'root', 'secret', {
    host:'127.0.0.1',
    dialect:'mysql'
});

const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);

// un Genero puede tener muchas peliculas
Genre.hasMany(Movie, {as:'movies'});
// una Pelicula puede tener un Genero
Movie.belongsTo(Genre, {as:'genre'});
// una Pelicula puede tener varios Directores
Movie.hasMany(Director, {as:'directors'});
// un Director puede tener muchas Peliculas
Director.hasMany(Movie, {as:'movies'})
// un Booking tiene un Miembro
Booking.belongsTo(Member, {as:'members'});
// un Booking tiene una Copia
Booking.belongsTo(Copy, {as:'copies'});
// un Miembro tiene muchos Bookings
Member.hasMany(Booking, {as:'bookings'});
// una Copia tiene muchos Bookings
Copy.hasMany(Booking, {as:'bookings'});
// un Actor participa en muchas Peliculas
MovieActor.belongsTo(Movie, {foreingKey: 'movieId'});
// en una Pelicula participan muchos Actores
MovieActor.belongsTo(Actor, {foreingKey: 'actorId'});

MovieActor.belongsToMany(Actor, {
    foreingKey: 'actorId',
    as: 'actors',
    through: 'moviesActors'
});

Actor.belongsToMany(Movie, {
    foreingKey: 'movieId',
    as: 'movies',
    through: 'moviesActors'
});

sequelize.sync({
    // Elimina todo y lo vuelve a lanzar
    force: true,
}).then(()=>{
    console.log("Base de datos actualizada");
});

module.exports = {Genre, Movie, Director, Actor, Copy, Booking, Member};