const express = require('express'); // importar express
const { Member } = require('../db');

function list(req, res, next) {
    Movie.findAll({}).then(objects => res.json(objects))
                                        .catch(err => res.send(err));
}

function index(req, res, next) {
    const id = req.params.id;
    movie.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function create(req, res, next) {
    const name = req.body.name;
    const lastname = req.body.lastname;
    const address = req.body.address;
    const phone = req.body.phone;
    const status = req.body.status;

    let member = new Object({
        name : name,
        lastname : lastname,
        address : address,
        phone : phone,
        status : status
    });

    Member.create(member)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function replace(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : "";
            const lastname = req.body.lastname ? req.body.lastname : "";
            const address = req.body.address ? req.body.address : "";
            const phone = req.body.phone ? req.body.phone : "";
            const status = req.body.status ? req.body.status : false;
            object.update({name:name, lastname:lastname, address:address, phone:phone, status:status})
                .then(member => res.json(member))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function update(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastname = req.body.lastname ? req.body.lastname : object.lastname;
            const address = req.body.address ? req.body.address : object.addresses;
            const phone = req.body.phone ? req.body.phone : object.phone;
            const status = req.body.status ? req.body.status : object.status;
            object.update({name:name, lastname:lastname, address:address, phone:phone, status:status})
                .then(member => res.json(member))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Member.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};