const express = require('express');
const { Director } = require('../db');

function list(req, res, next) {
    Director.findAll({include:['movies']}).then(objects => res.json(objects))
                                         .catch(err => res.send(err));
}

function index(req, res, next) {
    const id = req.params.id;
    director.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function create(req, res, next) {
    const name = req.body.name;
    const lastname = req.body.lastname;
    const movieId = req.body.movieId;

    let director = new Object({
        name: name,
        lastname: lastname,
        movieId : movieId
    });

    Director.create(director)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function replace(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : "";
            const lastname = req.body.lastname ? req.body.lastname : "";
            object.update({name:name, lastname:lastname})
                .then(director => res.json(director))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function update(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastname = req.body.lastname ? req.body.lastname : object.lastname;
            object.update({name:name, lastname:lastname})
                .then(director => res.json(director))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Director.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};