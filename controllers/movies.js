const express = require('express'); // importar express
const { Movie, Actor } = require('../db');

function list(req, res, next) {
    Movie.findAll({include:['genre', 'directors']}).then(objects => res.json(objects)) //El include desglosa el genre
                                            .catch(err => res.send(err));
}

function index(req, res, next) {
    const id = req.params.id;
    movie.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function create(req, res, next) {
    const titles = req.body.titles;
    const genreId = req.body.genreId;
    const directorId = req.body.directorId;

    let movie = new Object({
        titles: titles,
        genreId: genreId,
        directorId : directorId
    });

    Movie.create(movie)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function addActor(req, res, next) {
    const idMovies = req.body.idMovie;
    const idActor = req.body.idActor;

    Movie.findByPk(idMovie)
        .then(()=> {
            Actor,findByPk(idActor).then((actor)=>{
                movie.addActor(actor);
                res.json(movie);
            }).catch(err => res.send(err));
        }).catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id)
        .then((object) => {
            const titles = req.body.titles ? req.body.titles : "";
            object.update({titles:titles})
                .then(movie => res.json(movie))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function update(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id)
        .then((object) => {
            const titles = req.body.titles ? req.body.titles : object.titles;
            object.update({titles:titles})
                .then(movie => res.json(movie))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Movie.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};