const express = require('express');

function list(req, res, next) {
    res.send('respond withlist');
}
function index(req, res, next) {
    const id = req.params.id;
    res.send(`index Parameter => ${id}`);
}
function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    res.send(`Create => ${name} ${lastName}}`);
}
function replace(req, res, next) {
    res.send('respond with replace');
}
function update(req, res, next) {
    res.send('respond with update');
}
function destroy(req, res, next) {
    res.send('respond with destroy');
}

module.exports = {list, index, create, replace, update, destroy};