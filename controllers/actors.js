const express = require('express'); // importar express
const { Actor } = require('../db');

function list(req, res, next) {
    Actor.findAll({include:['members', 'copies']}).then(objects => res.json(objects))
                                        .catch(err => res.send(err));
}

function index(req, res, next) {
    const id = req.params.id;
    actor.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function create(req, res, next) {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copyId = req.body.copyId;

    let actor = new Object({
        date : date,
        memberId : memberId,
        copyId : copyId
    });

    Actor.create(actor)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}
function replace(req, res, next) {
    const id = req.params.id;
    Actor.findByPk(id)
        .then((object) => {
            const date = req.body.date ? req.body.date : "";
            object.update({date:date})
                .then(actor => res.json(actor))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function update(req, res, next) {
    const id = req.params.id;
    Actor.findByPk(id)
        .then((object) => {
            const date = req.body.date ? req.body.date : object.date;
            object.update({date:date})
                .then(actor => res.json(actor))
                .catch(err => res.send(err));
        }).catch(err => res.send(err));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Actor.destroy({where: {id:id}})
                .then(obj => res.json(obj))
                .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};